package net.cassite.search;

/**
 * connection methods.
 */
public enum Method {
        /**
         * get
         */
        GET,
        /**
         * post
         */
        POST
}
